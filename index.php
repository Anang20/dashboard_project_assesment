<?php

include 'crud/connection.php';

// include 'crud/function.php';

// $penampung=[];
// if (isset($_POST['cari']))
// {
//   $penampung=searching($_POST['search']);
//   if (!empty($penampung)) {
//     $data_daftar=$penampung;
//   } elseif (empty($penampung)) {
//     $error = "Maaf, ".$_POST['search']." Tidak ditemukan!!!";
//   }
// }
$row = $daftar->rowCount();

$totals = 0;
foreach($data_daftar as $num) {
    $totals+=$num['harga_tiket'];
}

$vip=$db->query("select tiket_kelas from tiket where tiket_kelas= 'VIP' ");
$vip2=$vip->fetchAll();
$temp_vip = count($vip2);

$patas=$db->query("select tiket_kelas from tiket where tiket_kelas= 'Patas' ");
$patas2=$patas->fetchAll();
$temp_patas = count($patas2);

$ekonomi=$db->query("select tiket_kelas from tiket where tiket_kelas= 'Ekonomi' ");
$ekonomi2=$ekonomi->fetchAll();
$temp_ekonomi = count($ekonomi2);

$executive=$db->query("select tiket_kelas from tiket where tiket_kelas= 'Executive' ");
$executive2=$executive->fetchAll();
$temp_executive = count($executive2);

$super_executive=$db->query("select tiket_kelas from tiket where tiket_kelas= 'Super Executive' ");
$super_executive2=$super_executive->fetchAll();
$temp_super = count($super_executive2);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Import icon fontawesome-->
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- Import Favicon -->
    <link rel="shortcut icon" href="image/bus.png" type="image/x-icon" class="rounded-circle">
    <!--Import dari materialize.css1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <title>Data Penumpang | BusKita</title>
</head>

<style type="text/css">
body {
    background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}
.cl {
  background: linear-gradient(to top left,#8a4592,#35b0a9);
}
nav{
	background-image: linear-gradient(to top right,#35b0a9,#8a4592); 
	padding-left: 300px;
}
.content{
	padding-left: 300px;
	height: 800px;
}
.card-bg{
	background: rgba(0,0,0,0);
}
@media only screen and (max-width: 992px){
	.content,nav{
		padding-left: 0;
	}
}
</style>

<!-- slide out -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

 <!-- navbar -->

<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center">Admin</a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>		
    </div>
  </nav>
</div>
    
    <!-- sidenav -->

<ul class="sidenav sidenav-fixed cl" id="slide-out">
	<li>
		<div class="user-view">
			<div class="background">
				<img src="image/bg.jpg" width="100%">
			</div>
				<a href="https://github.com/Anang20"><img src="image/pp.png" class="circle"></a>
				<a href="https://github.com/Anang20" class="white-text name">Anang Syah Amirul Haqim</a>
				<a href="mailto:anangsyah766@gmail.com" class="white-text email">anangsyah766@gmail.com</a>
		</div>	
  </li>
  <li><a href="index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Dashboard</a></li>
  <li><a href="data.php" class="white-text"><i class="fas fa-address-card fa-2x"></i>Data Penumpang</a></li>
	<li><a href="tambah.php" class="white-text"><i class="fas fa-user-plus fa-2x"></i>Tambah Data</a></li>
</ul>
   
<div class="content bg">
	<div class="container">
		<div class="row">
    <div class="col s12 m6 l4">
				<div class="card card-bg white-text">
					<div class="card-content center">
						<p>Total Penumpang</p>
						<h5><?= $row ?></h5>
						<b class="green-text">%12</b>
					</div>
				</div>
		</div>
			<div class="col s12 m6 l4">
				<div class="card card-bg white-text">
					<div class="card-content center">
						<p>Tiket Terjual</p>
						<h5><?= $row ?></h5>
						<b class="red-text">%10</b>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="card card-bg white-text">
					<div class="card-content center">
						<p>Penghasilan Tiket</p>
						<h5><?= "Rp.".number_format($totals) ?></h5>
						<b class="green-text">%7</b>
					</div>
				</div>
			</div>
      <div class="col l12 m6 s12">
				<div class="card card-bg">
					<div class="card-content" style="max-width: 50rem;">
						<h4 class="white-text" style="text-align: center;">Data Penjualan Tiket</h4>
						<canvas id="myChart"></canvas>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>

  <!-- Table -->

<div class="content bg white-text";>
    <div class="row">
        <div class="col s12">
            <div class="card card-bg">
                <table class="centered highlight">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Tiket Kelas</th>
                        <th>Tujuan</th>
                        <th>Harga Tiket</th>
                        <th>Keberangkatan</th>
                      </tr>
                    </thead>
                    <?php foreach ($data_daftar as $key): ?>
                    <tbody>
                      <tr>
                        <td><?= $key['nama_penumpang'];?></td>
                        <td><?= $key['tiket_kelas'];?></td>
                        <td><?= $key['tujuan'];?></td>
                        <td><?= "Rp.". number_format($key['harga_tiket']);?></td>
                        <td><?= $key['tanggal'];?></td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	var vip = <?php echo $temp_vip ?>
</script>
<script>
	var patas = <?php echo $temp_patas ?>
</script>
<script>
	var ekonomi = <?php echo $temp_ekonomi ?>
</script>
<script>
	var executive = <?php echo $temp_executive ?>
</script>
<script>
	var $super_executive = <?php echo $temp_super ?>
</script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['vip', 'patas', 'ekonomi', 'executive', 'super executive'],
        datasets: [{
            label: '# of Votes',
            data: [vip, patas, ekonomi, executive, $super_executive],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

</body>
</html>
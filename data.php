<?php

include 'crud/connection.php';
include 'crud/function.php';

if (isset($_GET['delete']))
{
  deletePenumpang($_GET);
  header('Location: data.php');
}

$penampung=[];
if (isset($_POST['cari']))
{
  $penampung=searching($_POST['search']);
  if (!empty($penampung)) {
    $data_daftar=$penampung;
  } elseif (empty($penampung)) {
    $error = "Maaf, ".$_POST['search']." Tidak ditemukan!!!";
  }
}
$row = $daftar->rowCount();

$totals = 0;
foreach($data_daftar as $num) {
    $totals+=$num['harga_tiket'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Import icon fontawesome-->
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- Import Favicon -->
    <link rel="shortcut icon" href="image/bus.png" type="image/x-icon" class="rounded-circle">
    <!--Import dari materialize.css1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <title>Data Penumpang | BusKita</title>
</head>

<style type="text/css">
body {
    background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}
.bg{
	background: linear-gradient(to top left,#8a4592,#35b0a9);
}
nav{
	background-image: linear-gradient(to top right,#35b0a9,#8a4592); 
	padding-left: 300px;
}
.content{
	padding-left: 300px;
	height: 800px;
}
.card-bg{
	background: rgba(0,0,0,0);
}
@media only screen and (max-width: 992px){
	.content,nav{
		padding-left: 0;
	}
}
</style>

<!-- slide out -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

 <!-- navbar -->

<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center">Admin</a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>		
    </div>
  </nav>
</div>
    
    <!-- sidenav -->

<ul class="sidenav sidenav-fixed bg" id="slide-out">
	<li>
		<div class="user-view">
			<div class="background">
				<img src="image/bg.jpg" width="100%">
			</div>
				<a href="https://github.com/Anang20"><img src="image/pp.png" class="circle"></a>
				<a href="https://github.com/Anang20" class="white-text name">Anang Syah Amirul Haqim</a>
				<a href="mailto:anangsyah766@gmail.com" class="white-text email">anangsyah766@gmail.com</a>
		</div>	
  </li>
  <li><a href="index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Dashboard</a></li>
  <li><a href="data.php" class="white-text"><i class="fas fa-address-card fa-2x"></i>Data Penumpang</a></li>
  <li><a href="tambah.php" class="white-text"><i class="fas fa-user-plus fa-2x"></i>Tambah Data</a></li>
</ul>

  <!-- Table -->

<div class="content bg white-text";">
    <div class="container">
      <div class="row">
          <form class="col s12" action="data.php" method="POST">
            <div class="row">
              <div class="input-field col s6">
                <input id="search" type="text" class="validate" name="search" placeholder="Search...">
              </div>
              <div class="input-field col s6">
                <button class="waves-effect waves-light btn-small indigo" type="submit" name="cari"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
    <div class="row">
        <div class="col s12">
            <div class="card card-bg">
                <table class="centered highlight">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Tiket Kelas</th>
                        <th>Tujuan</th>
                        <th>Harga Tiket</th>
                        <th>Keberangkatan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <?php foreach ($data_daftar as $key): ?>
                    <tbody>
                      <tr>
                        <td><?= $key['nama_penumpang'];?></td>
                        <td><?= $key['tiket_kelas'];?></td>
                        <td><?= $key['tujuan'];?></td>
                        <td><?= "Rp.". number_format($key['harga_tiket']);?></td>
                        <td><?= $key['tanggal'];?></td>
                        <td class="border rounded border border-secondary"><a class="btn btn-outline-danger" href="data.php?delete=&id_penumpang=<?php echo $key['id_penumpang']; ?> "onclick="return confirm('Apakah Anda yakin ingin menghapus data ini???')"><i class="fas fa-user-minus"></i></a>  <a class="btn btn-outline-success" href="edit.php?id_penumpang=<?php echo $key['id_penumpang']; ?>"><i class="fas fa-user-edit"></i></a></td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>
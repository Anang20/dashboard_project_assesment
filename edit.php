<?php

include "crud/connection.php";

$daftar=$db->query("select * from tiket where id_penumpang=".$_GET['id_penumpang']);
$data_daftar=$daftar->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <!--Import icon fontawesome-->
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- Import Favicon -->
    <link rel="shortcut icon" href="image/bus.png" type="image/x-icon" class="rounded-circle">
    <!--Import dari materialize.css1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <title>Beli Tiket | BusKita</title>
</head>

<style type="text/css">
body {
    background-image: linear-gradient(to top left,#8a4592,#35b0a9);
}
.bg{
	background: linear-gradient(to top left,#8a4592,#35b0a9);
}
nav{
	background-image: linear-gradient(to top right,#35b0a9,#8a4592); 
	padding-left: 300px;
}
.content{
	padding-left: 300px;
	height: 800px;
}
.card-bg{
	background: rgba(0,0,0,0);
}
@media only screen and (max-width: 992px){
	.content,nav{
		padding-left: 0;
	}
}
</style>

<!-- slide out -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

<!-- navbar -->

<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center">Admin</a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>		
    </div>
  </nav>
</div>

   <!-- sidenav -->

<ul class="sidenav sidenav-fixed bg" id="slide-out">
	<li>
		<div class="user-view">
			<div class="background">
				<img src="image/bg.jpg" width="100%">
			</div>
				<a href="https://github.com/Anang20"><img src="image/pp.png" class="circle"></a>
				<a href="https://github.com/Anang20" class="white-text name">Anang Syah Amirul Haqim</a>
				<a href="mailto:anangsyah766@gmail.com" class="white-text email">anangsyah766@gmail.com</a>
		</div>	
  </li>
  <li><a href="index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Dashboard</a></li>
  <li><a href="data.php" class="white-text"><i class="fas fa-address-card fa-2x"></i>Data Penumpang</a></li>
	<li><a href="tambah.php" class="white-text"><i class="fas fa-user-plus fa-2x"></i>Tambah Data</a></li>
</ul>

<div class="content bg">
	<div class="container">
		<div class="row">
      <form action="crud/update.php" method="POST">
        <div class="row">
            <h3 class="text-center white-text">Edit Data</h3>
            <input type="hidden" name="id_penumpang" value="<?php echo $data_daftar[0]['id_penumpang']; ?>">
          <div class="input-field col s12">
            <input type="text" class="white-text" name="nama_penumpang" value="<?php echo $data_daftar[0]['nama_penumpang']; ?>" class="materialize-textarea" autocomplete="off" required>
            <label for="textarea1" class="white-text">Nama</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <select name="tiket_kelas" value="<?php echo $data_daftar[0]['tiket_kelas']; ?>" autocomplete="off" required>
            <?php
            $data=['Ekonomi','Patas','VIP','Executive','Super Executive'];
            ?>
            <?php foreach($data as $key):?>
                <?php if($key==$data_daftar[0]['tiket_kelas']):?>
                <option selected><?php echo $key;?></option>
                <?php else:?>
                    <option><?php echo $key;?></option>
                <?php endif;?>
            <?php endforeach;?>
            </select>
            <label class="white-text">Tiket Kelas</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input type="text" class="white-text" name="tujuan" value="<?php echo $data_daftar[0]['tujuan']; ?>" autocomplete="off" class="materialize-textarea" required>
            <label for="textarea1" class="white-text">Tujuan</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <select name="harga_tiket" value="<?php echo number_format($data_daftar[0]['harga_tiket']); ?>" autocomplete="off" required>
            <?php
            $harga=[150000,300000,400000,450000,500000];
            ?>
            <?php foreach($harga as $key):?>
                <?php if($key==$data_daftar[0]['harga_tiket']):?>
                <option selected><?php echo $key;?></option>
                <?php else:?>
                    <option><?php echo $key;?></option>
                <?php endif;?>
            <?php endforeach;?>
            </select>
            <label class="white-text">Harga Tiket</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <label for="date" class="white-text">Tanggal Keberangkatan</label>
            <input type="text" class="white-text" name="tanggal"  value="<?php echo $data_daftar[0]['tanggal']; ?>" class="datepicker" autocomplete="off" required>
          </div>
        </div>
        <button type="submit" class="waves-effect waves-light btn" onclick="return confirm('Apakah Anda yakin untuk mengubah data ini???')">Edit</button>
      </form>
    </div>
  </div>
</div>

  <script>
    $(document).ready(function(){
    $('.datepicker').datepicker({
      dateFormat:"dd-mm-yy",
    });
  });


  $(document).ready(function() {
    M.updateTextFields();
  });

  $(document).ready(function(){
    $('select').formSelect();
  });
  </script>
</body>
</html>